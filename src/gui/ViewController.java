package gui;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.AnchorPane;

public class ViewController implements Initializable {

	@FXML
	private AnchorPane anchorPane;

	@FXML
	private MenuItem menuItemCadastrosClientes;

	@FXML
	private MenuItem menuItemGraficosVendasPorMes;

	@FXML
	private MenuItem menuItemProcessosVendas;

	@FXML
	private MenuItem menuItemRelatoriosQuantidadeDeProdutosEstoque;

	@Override
	public void initialize(URL url, ResourceBundle rb) {
		// TODO Auto-generated method stub

	}

}
